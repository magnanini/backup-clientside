<strong># README #</strong>

### Client-side part of my backup script ###

* Quick summary
* Version 1.0 Mainline
* [Manu Manuna](http://matnom.com)

### Basic Setup
[Main Variables]
backup_ip=
server_name= #This Name uses for folder name on backup server as well

backup_path=/home/backup/data/ #Path to save backup with slash!
backup_days=3                  # How long need to keep old backups

backup_user=backup # Need to BACKUP always 
mysql_user=backup
mysql_pass=
mysql_xtrabackup= # Xtrabackup.  Full hot backup 
[path]
List of folders to backup. One folder per line

[database]
... databases list, only one database per string

Rights for Backup Mysql user: REPLICATION CLIENT, RELOAD, LOCK TABLES, SELECT, CREATE_VIEW, TRIGGER and for Xtrabackup PROCESS