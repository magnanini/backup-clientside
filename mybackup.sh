#! /bin/bash
version="Client-side 1.0 22/01/2020"
if [ "$(whoami)" != 'root' ] ; then
     echo "Script allowed only under root privileges!";  exit 1
fi

STARTTIME=$(date +%s)

#Get script folder
SCRIPT_PATH="${BASH_SOURCE[0]}";
if ([ -h "${SCRIPT_PATH}" ]) then
  while([ -h "${SCRIPT_PATH}" ]) do SCRIPT_PATH=`readlink "${SCRIPT_PATH}"`; done
fi
pushd . > /dev/null
cd `dirname ${SCRIPT_PATH}` > /dev/null
SCRIPT_PATH=`pwd`;
popd  > /dev/null

filename="$SCRIPT_PATH/backup.conf"

if [ ! -f "$filename" ]; then
echo "Error opening config file: $filename"
exit 1
fi

declare -a config=( `cat "$filename"`)
prefix="backup_"

date_format="+%d-%m-%Y"
date_format1="+%d\-%m\-%Y"
FOLDER_TODAY=`date $date_format`
TODAY=`date +%d%m%Y` #Filename
DAY=`date +%d`
makedump=0
backup_path=( ` grep backup_path "$filename" |  sed 's/^.*=//g'  `)
today_backup_folder="$backup_path${prefix}$FOLDER_TODAY"

backup_days=( ` grep backup_days "$filename" |  sed 's/^.*=//g'  `)

backup_compress=( ` grep backup_compress "$filename" |  sed 's/^.*=//g'  `)
backup_debug=( ` grep backup_debug "$filename" |  sed 's/^.*=//g'  `)
debug="Debug output"

backup_user=( ` grep backup_user "$filename" |  sed 's/^.*=//g'  `)
server_name=( ` grep server_name "$filename" |  sed 's/^.*=//g'  `)
server_name=${server_name// /} # Remove whitespaces

mysql_xtrabackup=( ` grep mysql_xtrabackup "$filename" |  sed 's/^.*=//g'  `); test $mysql_xtrabackup  ||  mysql_xtrabackup=0
mysql_user=( ` grep mysql_user "$filename" |  sed 's/^.*=//g'  `)
mysql_pass=( ` grep mysql_pass "$filename" |  sed 's/^.*=//g'  `)
mysql_host=( ` grep mysql_host "$filename" |  sed 's/^.*=//g'  `)

list_database=( ` cat "$filename" |  sed -n -e '/^\[database\]$/,/^</{ /^\[database\]$/d; /^\[.*\]/d; /^#/d; /^\s*$/d; s/\s//; s/#.*//; p; }'  `)
list_path=( ` cat "$filename" |  sed -n -e '/^\[path\]$/,/^\[.*\]/{ /^\[path\]$/d; /^\[/d; /^#/d; /^\s*$/d; s/\s//; s/#.*//; p; }'  `)
list_exclude_dir=( ` cat "$filename" |  sed -n -e '/^\[exclude\]$/,/^\[.*\]/{ /^\[exclude\]$/d; /^\[/d; /^#/d; /^\s*$/d; s/\s//; s/#.*//; s/^/--exclude=/; p; }'  `)

function join { local IFS="$1"; shift; echo "$*"; }
EXCLUDE=$(join " " "${list_exclude_dir[@]}")
FILENAME="$server_name""_""$TODAY.tar.gz"
TARFOLDERS=$(join " " "${list_path[@]}")
COUNTER=0 #DB counter

#
### Functions ##########
#
error() {
        echo $1
        exit $?
}
is_on() {
if [ $1 ==  "true" ]; then return 1
elif [ $1 = 1 ]; then return 1
else return 0
fi
}
makeDir() {
    [ ! -d "$1" ] &&  mkdir -p "$1"
    chown -R "$2":"$2" "$backup_path"
    test "$3" = "secure" && chmod 700 "$1"
}

RotateDir() {
        declare -a keep_folders
        declare -a dirs

        # make array of folders to keep
        for i in `seq $(($backup_days -1 )) -1  0`;     do
        keep_folders+=(" -name ")
        keep_folders+=("$prefix$(date  --date="${i} days ago" $date_format)")
        keep_folders+=(" -prune -o ")
        done
        prune=$(join "" "${keep_folders[@]}")

        echo "Skip backup folders: $prune"
        echo ""
        echo "We will keep backups for $backup_days days"
        echo "Following folders will be deleted:"
        dirs=( $(find $backup_path -maxdepth 1  ${prune} -type d -name ${prefix}'*' -printf '%P\n' ) )
        for((ii=0;ii<=(${#dirs[@]}-1);ii++))
        do
        echo $ii ":path: ${backup_path}${dirs[ii]}"
                rm -fr "${backup_path}${dirs[ii]}"
        done

}

checkFile() {
    if [ ! -f "$1" ] ; then
         touch "$1" || debug+=":Can't create file $1!\n"
    fi
    chown "$2":"$2" "$1" || debug+=":Wrong user - $2 \n"
    test "$3" = "secure" &&  chmod 600 "$1"
}

#Mysqldump function
make_mysqldump() {
if [ $mysql_xtrabackup = 1 ] ; then
XtraFolder="$today_backup_folder/Xtrabackup""_""$TODAY"

[ -d "$XtraFolder" ] && error "Error! Xtrabackup folder already exists - $XtraFolder"

echo "Processing full database backup with Xtrabackup...."
echo "Destanation folder is "$XtraFolder
innobackupex   --user=${mysql_user} --password=${mysql_pass} --no-timestamp   --throttle=40  --rsync ${XtraFolder} 2>&1 &&  innobackupex   --user=${mysql_user} --passwor=${mysql_pass} --no-timestamp   --throttle=40 --apply-log ${XtraFolder} 2>&1
echo "Done."
tarline=" cfz $today_backup_folder/Xtrabackup_$TODAY.tar.gz -C $XtraFolder/ . "
is_on $backup_compress; test $? = 1 && tar $tarline >/dev/null 2>&1 && rm -r $XtraFolder && echo "Compressed."
else

echo "Processing databases backup with Mysqldump...(${#list_database[@]})::"
for database in "${list_database[@]}"
do
        COUNTER=$[$COUNTER +1]
        echo "$COUNTER: Processing database - $database..."
        DUMPFILENAME="$today_backup_folder/sql_$database""_""$TODAY"                                                                                                                                                
        mysqldump -u${mysql_user} -p${mysql_pass} -h${mysql_host} ${database} --single-transaction --quick --lock-tables=false  >${DUMPFILENAME}                                                                    
        is_on $backup_compress; test $? = 1 && echo -n "Compressing..." && tar cfz "$DUMPFILENAME.tar.gz" -C $today_backup_folder "sql_$database""_""$TODAY" && rm -r $DUMPFILENAME && echo "OK"                    
        echo "Done"
done
fi

}

# Backup folders
make_tar() {
        echo ""
        echo -n "Backuping following folders: $(join "," "${list_path[@]}")"
        echo -n " "
        tar cvfz "$today_backup_folder/$FILENAME"  ${TARFOLDERS} ${EXCLUDE} >/dev/null 2>&1
        echo " :: Done"
}
function elapsed() {
    diff=$(($(date +%s)-$1))
    echo $(printf '%dh:%dm:%ds\n' $(($diff/3600)) $(($diff%3600/60)) $(($diff%60)))
}
#Create log file
make_log() {
if [ -z "$1" ]; then
cat >> "$backup_path"logfile.log <<_EOF
===============================================
  Backup done on: $FOLDER_TODAY at `date +%H:%M`
  Time elapsed: $(elapsed $STARTTIME)
===============================================
Database backuped: $COUNTER
Folders: $(join "," "${list_path[@]}")
_EOF
else
cat >> "$backup_path"logfile.log <<_EOF
======================
Critical error
`date +%H:%M`
=====================
Error: $1
_EOF
fi
chown $backup_user ${backup_path}logfile.log
}

# Make defaults
test $backup_compress || backup_compress=0

# Let's start!
echo "###########################"
echo "Backup script $version"
echo "###########################"
echo ""
# Check if Backuser exists
[ -z "$(getent passwd $backup_user)" ] && error "Incorrect backup user!"

if [ "$mysql_xtrabackup" == "1" ]; then
                if [  -n "$(uname -a | grep Ubuntu)" ]; then
                        [ $(dpkg-query -W -f='${Status}' xtrabackup 2>/dev/null | grep -c 'ok installed') != 1  ] &&  error "Xtrabackup Not installed!"
                else 
                        [ -z "$(rpm -qa | grep xtrabackup)" ] && error "Xtrabackup Not installed!"
                fi

        echo "Xtrabackup installed."
fi

# Check if today's backup already created
[ -d "$today_backup_folder" ] && error "Backup already exists"

makeDir $today_backup_folder $backup_user secure
test ${#list_database[@]} != 0  && makedump=1; test $mysql_xtrabackup = 1 &&  makedump=1
test $makedump = 1 && make_mysqldump

if [ "$list_path" != "" ]; then
       make_tar
echo "tar"
fi

RotateDir

make_log

chown -R "$backup_user:" "$today_backup_folder"

test $backup_debug = "on"  && echo "$debug"

echo "Time elapsed: $(elapsed $STARTTIME)"